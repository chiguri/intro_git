SHELL = /bin/bash


.PHONY: clean all


all : intro.pdf ;



clean :
	rm -f *.dvi *.aux *.log intro.pdf


intro.pdf : intro.tex
	platex intro
	platex intro
	dvipdfmx intro
