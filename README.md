* 版管理とgit超入門

[![wercker status](https://app.wercker.com/status/5a9d5eefacbbacba742e7c76b630cc71/m/master "wercker status")](https://app.wercker.com/project/byKey/5a9d5eefacbbacba742e7c76b630cc71)

- あくまで簡単な操作しか説明しません。細かな話は気が向いたら増やすかもしれませんが、基本的には他の本（Git入門とかそういった本）を参照してください。
- 環境はかなり関西学院大学（特に理工学部情報科学科・人間システム工学科）を意識していますが、プロキシなどの話以外は基本的に違いがありません。
- ファイルはUTF-8にしています。
